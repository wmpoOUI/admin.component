import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import cx from 'classnames';
import { HOST_ADMIN } from 'library/API';
import styles from './Tab.module.scss';

const Tab = ({
  className,
  tabList = [{ id: '', title: '', host: '', link: '' }],
  tabTool,
  defaultSelected,
  callback,
  children,
}) => {
  const [selected, setSelected] = useState(defaultSelected);

  const handleClick = (e, id) => {
    if (typeof callback === 'function') {
      if (callback(id)) {
        setSelected(id);
      } else {
        // <Link /> 인 경우. 리액트 특성상 이벤트를 막아줘야 페이지 이동을 안한다.
        e.preventDefault();
      }
    } else {
      setSelected(id);
    }
  };

  useEffect(() => {
    setSelected(defaultSelected);
  }, [defaultSelected]);

  return (
    <>
      <div
        className={cx(styles.wrapper, {
          [className]: className,
        })}
      >
        <div className={styles.inner}>
          <ul role="tablist">
            {tabList.map(tab => {
              const { id, title, host, link, state } = tab;
              const { pathname } = window.location;

              if (host === 'HOST_ADMIN') {
                return (
                  <li key={id}>
                    <a
                      href={HOST_ADMIN + link}
                      className={styles.tab}
                      role="tab"
                      aria-selected={id === selected && link === pathname}
                    >
                      {title}
                    </a>
                  </li>
                );
              }

              if (host === 'HOST_ADMIN2') {
                return (
                  <li key={id}>
                    <Link
                      to={{ pathname: link, state }}
                      className={styles.tab}
                      role="tab"
                      aria-selected={id === selected && link === pathname}
                      onClick={e => handleClick(e, id)}
                    >
                      {title}
                    </Link>
                  </li>
                );
              }

              return (
                <li key={id}>
                  <button
                    type="button"
                    className={styles.tab}
                    role="tab"
                    aria-selected={id === selected}
                    onClick={e => handleClick(e, id)}
                  >
                    {title}
                  </button>
                </li>
              );
            })}
          </ul>
          {tabTool && <div className={styles.tool}>{tabTool}</div>}
        </div>
      </div>
      {Array.isArray(children)
        ? children.map(node => {
            const { id } = node.props;
            if (id === selected) return <React.Fragment key={id}>{node}</React.Fragment>;
            return null;
          })
        : children}
    </>
  );
};

export default Tab;
