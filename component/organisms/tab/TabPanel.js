import React from 'react';

const TabPanel = ({ id = '', className = 'panel-comm', children }) => {
  return (
    <div id={id} className={className} role="tabpanel">
      {children}
    </div>
  );
};

export default TabPanel;
