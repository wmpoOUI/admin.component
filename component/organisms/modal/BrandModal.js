import React, { useState, useEffect } from 'react';
import { HOST_API, API } from 'library/API';
import { SelectScrollList } from 'components/molecules';
import { Debug } from 'library/Debug';
import Modal from './Modal';
import styles from './BrandModal.module.scss';

const callBrandList = params => {
  const method = 'GET';
  const url = `${HOST_API}/admin/brands`;
  return API({ method, url, params });
}; // 브랜드 (프랜차이즈) 목록

const BrandModal = ({ id, name, title, style, value, onChange, show, showModal }) => {
  const [state, setState] = useState({ brands: [], list: [], isAPICall: true });
  const { list, isAPICall } = state;
  const isEmpty = state.brands.length === 0;

  const setList = brands => {
    const list = brands.map(data => {
      let checked = false;
      value.forEach(value => {
        if (value[id] * 1 === data[id] * 1) checked = true;
      });
      return { ...data, label: data.name, value: data.ID, checked };
    });
    setState({ ...state, brands, list, isAPICall: false });
  };

  const getBrandList = async () => {
    const response = await callBrandList({ limit: 9999 });
    if (response) {
      const { result, data } = response.data;
      if (result.code === 1) {
        const _data = data.map(d => ({ ...d, name: d.name, brandID: d.ID }));
        setList(_data);
      } else {
        window.alert('담당 개발자에게 문의주세요.');
      }
    }
  };

  const handleChange = (e, idx, checked) => {
    const list = [...state.list];
    list[idx] = { ...list[idx], checked: !checked };
    setState({ ...state, list });
  };

  const handleChecked = e => {
    const { name } = e.target;
    if (state.brands.length === 0) return;
    const list = state.list.map(data => ({ ...data, checked: name === 'checked' }));
    setState({ ...state, list });
  };

  const onUpdateChange = (e, name, list) => {
    if (list.length < 1) {
      alert('프랜차이즈를 선택해주세요.');
      return false;
    }
    onChange(e, name, list);
    showModal(false);
  };

  const footerBtn = [
    {
      name: '등록',
      className: 'btn-slate',
      callback: e => {
        const list = state.list.filter(data => data.checked);
        Debug.log('[brands callback]: ', [...list]);
        onUpdateChange(e, name, list);
      },
    },
  ];

  useEffect(() => {
    if (show) setList(state.brands);

    /* 최초만 시/도 목록 조회 */
    if (!isAPICall) return;
    if (show) getBrandList();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [show]);

  return (
    <Modal title={title} style={style} footerBtn={footerBtn} show={show} showModal={showModal}>
      <div className={`btn-group ${styles.button}`}>
        <button type="button" name="checked" className="btn-blue" disabled={isEmpty} onClick={handleChecked}>
          전체 선택
        </button>
        <button type="button" name="unchecked" className="btn-red" disabled={isEmpty} onClick={handleChecked}>
          전체 해제
        </button>
      </div>
      <SelectScrollList list={list} onChange={handleChange} />
    </Modal>
  );
};

BrandModal.defaultProps = {
  id: 'ID',
  name: '',
  title: '노출 프랜차이즈 설정',
  style: { minWidth: 500 },
  value: [],
  footerBtn: [{ name: '등록', className: 'btn-slate', callback: () => {} }],
  show: false,
  onChange: () => {},
  showModal: () => {},
};

export default BrandModal;
