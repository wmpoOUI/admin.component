import React, { useState, useEffect } from 'react';
import { Input } from 'components/molecules';
import { Debug } from 'library/Debug';
import { getRegionSiDo, getRegionGuGun, getRegionDong } from 'library/RegionsAPI';

import Modal from './Modal';
import styles from './RegionModal.module.scss';

const defState = {
  sido: '',
  sidos: [{ label: '시/도', value: '', disabled: true }],
  gugun: '',
  guguns: [{ label: '구/군', value: '', disabled: true }],
  dong: [],
  dongs: [],
  regions: [],
  isAPICall: true,
};

const RegionModal = ({ id, name, title, style, value, onChange, show, showModal }) => {
  const [state, setState] = useState({ ...defState, regions: value });
  const isEmpty = state.dongs.length === 0;

  /* 시/도 목록 조회 */
  const getRegionSiDoList = async () => {
    const response = await getRegionSiDo();

    if (response) {
      const { result, data } = response.data;

      if (result.code === 1) {
        const sidos = data.map(data => ({ label: data.name, value: data.code }));
        sidos.unshift({ label: '시/도', value: '', disabled: true });

        setState({ ...state, sidos, isAPICall: false });
      } else {
        window.alert('담당 개발자에게 문의주세요.');
      }
    }
  };

  /* 구/군 목록 조회 */
  const getRegionGuGunList = async sido => {
    if (sido === '') return;
    const response = await getRegionGuGun(sido);

    if (response) {
      const { result, data } = response.data;

      if (result.code === 1) {
        const guguns = data.map(data => ({ label: data.name, value: data.code }));
        guguns.unshift({ label: '구/군', value: '', disabled: true });

        setState({ ...state, sido, gugun: '', guguns, dongs: [] });
      } else {
        window.alert('담당 개발자에게 문의주세요.');
      }
    }
  };

  /* 동 정보 조회 */
  const getRegionDongList = async gugun => {
    if (gugun === '') return;
    const response = await getRegionDong(state.sido, gugun);

    if (response) {
      const { result, data } = response.data;

      if (result.code === 1) {
        const dongs = data.map(data => ({ ...data, label: data.fullName, value: data.code }));

        setState({ ...state, gugun, dong: [], dongs });
      } else {
        window.alert('담당 개발자에게 문의주세요.');
      }
    }
  };

  const handleChange = (e, name, value) => {
    if (name === 'sido') {
      if (value === '') {
        setState({ ...defState, sido: value, sidos: state.sidos });
        return;
      }
      getRegionGuGunList(value);
    }

    if (name === 'gugun') {
      if (value === '') {
        setState({
          ...defState,
          sido: state.sido,
          sidos: state.sidos,
          gugun: value,
          guguns: state.guguns,
        });
        return;
      }
      getRegionDongList(value);
    }

    if (name === 'dong') {
      const dongs = state.dongs.map(data => ({
        ...data,
        checked: data.value === value ? !data.checked : data.checked,
      }));

      setState({ ...state, dongs });
    }
  };

  const handleChecked = e => {
    const { name } = e.target;

    if (isEmpty) return;
    const dongs = state.dongs.map(data => ({ ...data, checked: name === 'checked' }));
    setState({ ...state, dongs });
  };

  const handleRemove = ID => {
    const regions = state.regions.filter(data => data[id] !== ID);
    setState({ ...state, regions });
  };

  const footerBtn = [
    {
      name: '등록',
      className: 'btn-slate',
      callback: e => {
        const dongs = [];

        state.dongs.forEach(data => {
          const { ID, code, fullName, polygon } = data;

          if (data.checked) {
            const isCode = state.regions.some(data => data[id] === ID);
            if (!isCode) dongs.push({ [id]: ID, code, name: fullName, polygon });
          }
        });

        Debug.log('[regions callback]: ', [...state.regions, ...dongs]);

        onChange(e, name, [...state.regions, ...dongs]);
        setState({ ...defState, sidos: state.sidos, isAPICall: false });
        showModal(false);
      },
    },
  ];

  useEffect(() => {
    if (show) setState({ ...state, regions: value });

    /* 최초만 시/도 목록 조회 */
    if (!state.isAPICall) return;
    if (show) getRegionSiDoList();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [show]);

  return (
    <Modal title={title} style={style} footerBtn={footerBtn} show={show} showModal={showModal}>
      <Input type="select" id="sido" name="sido" value={state.sido} options={state.sidos} onChange={handleChange} />
      <Input
        type="select"
        id="gugun"
        name="gugun"
        disabled={state.sido === ''}
        value={state.gugun}
        options={state.guguns}
        onChange={handleChange}
      />
      <div className={`btn-group ${styles.button}`}>
        <button type="button" name="checked" className="btn-blue" disabled={isEmpty} onClick={handleChecked}>
          전체 선택
        </button>
        <button type="button" name="unchecked" className="btn-red" disabled={isEmpty} onClick={handleChecked}>
          전체 해제
        </button>
      </div>
      <ul className={`box-comm ${styles.box}`}>
        {state.dongs.map((node, i) => {
          const { label, value, checked } = node;
          return (
            <li key={value || i} className="inp-group">
              <input type="text" value={label} readOnly />
              <button
                type="button"
                className={checked ? `btn-slate` : `btn-default`}
                onClick={e => {
                  handleChange(e, 'dong', value);
                }}
              >
                선택
              </button>
            </li>
          );
        })}
      </ul>
      <ul className={`box-comm ${styles.box}`}>
        {state.regions.map((data, row) => {
          const ID = data[id];
          const { name } = data;

          return (
            <li key={ID || row} className="inp-group">
              <input type="text" value={name} readOnly />
              <button type="button" className="btn-red" onClick={() => handleRemove(ID)}>
                삭제
              </button>
            </li>
          );
        })}
      </ul>
    </Modal>
  );
};

RegionModal.defaultProps = {
  id: 'ID',
  name: '',
  title: '노출 지역 설정',
  style: { minWidth: 500 },
  value: [],
  footerBtn: [{ name: '등록', className: 'btn-slate', callback: () => {} }],
  show: false,
  onChange: () => {},
  showModal: () => {},
};

export default RegionModal;
