import React, { useEffect, useState } from 'react';
import cx from 'classnames';
import { SelectScrollList } from 'components/molecules';
import styles from './SelectScrollListForm.module.scss';

const SelectScrollListForm = ({ title, className, height, list, onSelectChange, onSelectAll, onUnSelectAll }) => {
  const [_list = [], setList] = useState(list);

  useEffect(() => {
    setList(list);
  }, [list]);

  return (
    <div className="panel-comm">
      <div className="head-panel">
        {title && (
          <strong className="tit-panel">
            <i className="fa fa-info-circle" /> {title}
          </strong>
        )}
        <div className="tool-panel">
          <div className="btn-group">
            <button type="button" name="checked" className="btn-blue" onClick={onSelectAll}>
              전체 선택
            </button>
            <button type="button" name="unchecked" className="btn-red" onClick={onUnSelectAll}>
              전체 해제
            </button>
          </div>
        </div>
      </div>
      <div className={`body-panel ${styles.body}`}>
        <SelectScrollList
          className={cx(styles.list, {
            [className]: className,
          })}
          height={height}
          list={_list}
          onChange={onSelectChange}
        />
      </div>
    </div>
  );
};

export default SelectScrollListForm;
