const MenuIcon = title => {
  switch (title) {
    case '프랜차이즈 관리':
      return 'fa fa-star';
    case '멤버조회':
      return 'fa fa-user';
    case '매장 및 심사':
      return 'fa fa-gavel';
    case '프로모션 관리':
      return 'fa fa-gift';
    case '딜 기획전 관리':
      return 'fa fa-paste';
    case '카테고리 관리':
      return 'fa fa-list';
    case '티켓 관리':
      return 'fa fa-ticket-alt';
    case '마케팅관리':
      return 'fa fa-bullhorn';
    case '구매 조회':
      return 'fa fa-shopping-cart';
    case '매출관리':
      return 'fa fa-chart-line';
    case '정산관리':
      return 'fa fa-calculator';
    case '운영관리':
      return 'fa fa-cog';
    case '쿠폰관리':
      return 'fa fa-puzzle-piece';
    case '게시판관리':
      return 'fa fa-edit';
    case '사이트 관리':
      return 'fa fa-sitemap';
    case '모니터링':
      return 'fa fa-desktop';
    case 'region':
      return 'fa fa-map-marker-alt';
    default:
      return 'fa fa-list';
  }
};

export default MenuIcon;
