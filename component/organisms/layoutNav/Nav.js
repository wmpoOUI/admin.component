/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import cx from 'classnames';
import { API, HOST_API, HOST_ADMIN, HOST_ADMIN2, HOST_AGENCY, isSuccess } from 'library/API';
import { Debug } from 'library/Debug';
import './Nav.scss';

const TempLink = ({ to, host, url, className, children }) => {
  if (host === 'HOST_ADMIN2') {
    return (
      <Link to={to} className={className}>
        {children}
      </Link>
    );
  }

  return (
    <a href={url || '#'} className={className}>
      {children}
    </a>
  );
};

const Menu = ({ data, onClick }) => {
  const { title, icon, link, host, url, sub, active, expanded, open } = data;

  if (sub) {
    return (
      <li
        className={cx({
          active,
          expanded,
        })}
      >
        <button type="button" className="btn-menu" title={title} onClick={onClick}>
          <span className="ico-menu">
            <i className={`fa fa-${icon ? `${icon}` : 'list'}`} />
          </span>
          <span className="txt-menu">{title}</span>
          <span className="ico-arr">
            <i className="fa fa-angle-right" />
          </span>
        </button>
        <ul data-submenu-title={title} className="list-menu">
          {sub.map((data, key) => {
            return <Menu key={key} data={data} />;
          })}
        </ul>
      </li>
    );
  }
  const newOpenWindow = () => {
    window.open(url, '_blank');
  };

  return (
    <li
      className={cx({
        active,
      })}
    >
      {open ? (
        <button
          type="button"
          className="link-menu"
          onClick={e => {
            newOpenWindow(e);
          }}
        >
          {title}
        </button>
      ) : (
        <TempLink to={link} host={host} url={url} className="link-menu">
          {title}
        </TempLink>
      )}
      {/* <Link to={link} className="link-menu">
        {title}
      </Link> */}
    </li>
  );
};

const Nav = () => {
  const navState = window.sessionStorage.getItem('navState');
  const fold = navState === 'true';
  const [data, setData] = useState([]);

  const handleClick = e => {
    const { classList } = document.getElementById('wmpoGnb');

    if (window.innerWidth > 960 && classList.contains('fold')) return;

    const { currentTarget } = e;
    const { title } = currentTarget;

    const newData = data.map(menu => {
      return { ...menu, expanded: menu.title === title ? !menu.expanded : false };
    });

    setData(newData);
  };

  const handleClose = () => {
    const { classList } = document.getElementById('wmpoGnb');

    classList.remove('open');
  };

  const setMenuList = menuData => {
    const { pathname } = window.location;

    const newData = menuData.map(menu => {
      let active = false;
      let expanded = false;

      const sub = menu.sub.map(sub => {
        if (sub.link === pathname) {
          active = true;
          expanded = true;
        }
        let urlLink = '';
        if (sub.host === 'HOST_ADMIN') {
          urlLink = HOST_ADMIN + sub.link;
        } else if (sub.host === 'HOST_ADMIN2') {
          urlLink = HOST_ADMIN2 + sub.link;
        } else {
          urlLink = HOST_AGENCY + sub.link;
        }
        return {
          ...sub,
          url: urlLink,
          active: sub.link === pathname,
        };
      });

      return { ...menu, sub, active, expanded };
    });

    setData(newData);
  };

  const getMenuList = async () => {
    const response = await API({ method: 'GET', url: `${HOST_API}/admin/me` });
    if (isSuccess(response)) {
      const { data, result } = response.data;
      if (result.code === 401) {
        window.alert('로그인이 다시필요합니다.');
        window.location.href = HOST_ADMIN;
      }
      window.sessionStorage.setItem('menu', JSON.stringify(data.menu));
      window.sessionStorage.setItem(
        'manager',
        JSON.stringify({
          ID: data.ID,
          loginID: data.loginId,
          name: data.name,
        }),
      );

      setMenuList(data.menu);
      return;
    }
    Debug.error('[getMenuList] 서버에서 데이터를 받아오지 못했습니다.');
  };

  useEffect(() => {
    // 사용안하는 localStorage 데이터 삭제
    window.localStorage.removeItem('menu');
    window.localStorage.removeItem('manager');

    const menu = JSON.parse(window.sessionStorage.getItem('menu'));
    if (!menu || menu === 'undefined') {
      getMenuList();
      return;
    }
    setMenuList(menu);

    const manager = JSON.parse(window.sessionStorage.getItem('manager'));
    if (!manager || manager === 'undefined' || !manager.ID) {
      getMenuList();
    }
  }, []);

  return (
    <nav id="wmpoGnb" className={fold ? 'fold' : ''}>
      <h2 className="screen-out">메뉴</h2>
      <div className="head-gnb">
        <button type="button" className="btn-close" onClick={handleClose}>
          <i className="fa fa-times" />
          <span className="screen-out">닫기</span>
        </button>
      </div>
      <ul className="list-gnb">
        <li className="tit-menu">
          <i className="fa fa-ellipsis-h" />
          <span className="txt-menu">
            어드민 <small>Beta 2.0</small>
          </span>
        </li>
        {data &&
          data.length > 0 &&
          data.map((data, key) => {
            return <Menu key={key} data={data} onClick={handleClick} />;
          })}
      </ul>
    </nav>
  );
};

export default Nav;
