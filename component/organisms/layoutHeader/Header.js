import React, { useState, useEffect } from 'react';
import { HOST_ADMIN } from 'library/API';
import { getSessionStorage } from 'library/Utils';
import { Dropdown } from 'components/molecules';
import HeaderLayout from './HeaderLayout';
import styles from './Header.module.scss';

const Header = () => {
  const navState = window.sessionStorage.getItem('navState');
  const [fold, setFold] = useState(navState === 'true');
  const [open, setOpen] = useState(false);
  const manager = getSessionStorage('manager');
  let name = '';

  if (manager) {
    name = manager.name;
  }

  const className = {
    wrapper: styles.user,
    button: styles.button,
  };

  const handleClick = () => {
    const { innerWidth } = window;

    if (innerWidth > 960) {
      setFold(!fold);
    } else {
      setOpen(!open);
    }
  };

  const logout = () => {
    window.sessionStorage.removeItem('menu');
    window.location.href = `${HOST_ADMIN}/manager/signout`;
  };

  useEffect(() => {
    const { classList } = document.getElementById('wmpoGnb');

    if (fold) {
      classList.add('fold');
    } else {
      classList.remove('fold');
    }

    window.sessionStorage.setItem('navState', fold);
  }, [fold]);

  useEffect(() => {
    const { classList } = document.getElementById('wmpoGnb');

    if (open) {
      classList.add('open');
    } else {
      classList.remove('open');
    }
  }, [open]);

  return (
    <HeaderLayout>
      <button type="button" className={styles.toggle} onClick={handleClick}>
        <i className="fa fa-bars" />
        <span className="screen-out">접기 &#47; 펼치기</span>
      </button>
      <div className={styles.tools}>
        <Dropdown name={name || ''} className={className}>
          <a href={`${HOST_ADMIN}/manager/password`}>
            <i className="fa fa-cog" />
            비밀번호 변경
          </a>
          <span className={styles.divider} />
          <button type="button" onClick={logout}>
            <i className="fa fa-power-off" />
            로그아웃
          </button>
        </Dropdown>
      </div>
    </HeaderLayout>
  );
};

export default Header;
