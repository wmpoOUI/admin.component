import React, { useEffect, useState } from 'react';
import { isExistWordInString, randomID } from '../../../library/Utils';
import { Debug } from '../../../library/Debug';

/**
 * Table
 * @param {array} columns *필수
 * @param {string} noMessage - 검색 결과 없을때 보여주는 텍스트
 *
 * --------------------------------
 *    columns attributes
 * --------------------------------
 * @attr {string} name - <TH> 텍스트
 * 컬럼명에 <br /> 을 넣고싶으면 '\n' 룰 추가한다. ex) { name: '업주ID\n(판매자번호)' },
 *
 * @attr {string} width - colgroup 에 width 를 지정한다.
 * ex) { width: '10%' } { width: '50px' }
 *
 * @attr {component} form - th 에 form 을 넣는다.
 * ex) { form: <CheckboxInput /> }
 *
 * @example
 const columns = [{
    name: '선택',
    width: '50px',
    form: <CheckboxAllInput id="inpCheckAll" className="lab-check" checked={data.isAllChecked} onClick={checkAll} />,
  },
 {
    name: 'No.',
    width: '10%',
 },
 { name: '매장번호' },
 { name: '매장명' },
 { name: '업주ID\n(판매자번호)' },
 { name: '매장분류' },
 { name: '주소' }]

 <Table columns={columns}>{tableBody}</Table>
 **/

const ColumnTable = info => {
  const [data, setData] = useState({
    id: 'table',
    columns: [],
    noMessage: '추가된 내역이 없습니다.',
  });

  useEffect(() => {
    if (info) {
      if (!info.columns) {
        Debug.error(`ColumnTable 컴포넌트에는 columns attribute 가 필요합니다.`);
        return;
      }
    }
    setData(info);
  }, [info]);

  // 태그에 <br /> 을 넣어준다.
  const breakLine = words => {
    const splitColumnName = words.split('\n');
    return splitColumnName.reduce((acc, curr, idx) => {
      if (idx === splitColumnName.length - 1) {
        return (
          <>
            {acc}
            {curr}
          </>
        );
      }

      return (
        <>
          {acc}
          {curr}
          <br />
        </>
      );
    }, '');
  };

  const thColumnGroups = () => {
    if (!data.columns) {
      return <col key={`thColumnGroup${0}`} />;
    }
    return data.columns.map(({ width = null }, index) => {
      return <col key={`thColumnGroup${index}`} width={width} />;
    });
  };

  const thColumns = () => {
    let text;
    let form;

    return data.columns.map(column => {
      text = column.name || '';
      form = column.form || null;

      // <br /> 이 필요한 경우
      if (text && isExistWordInString(text, '\n')) {
        text = breakLine(text);
      }

      return (
        <th key={randomID()} className="txt-center">
          {form || <span>{text}</span>}
        </th>
      );
    });
  };

  const noDataColumn = () => {
    return (
      <tr>
        <td colSpan={data.columns.length} className="txt-center">
          {data.noMessage}
        </td>
      </tr>
    );
  };

  return (
    <div className="tbl-comm">
      <table id={data.id}>
        <colgroup>{thColumnGroups()}</colgroup>
        <thead>
          <tr>{thColumns()}</tr>
        </thead>
        <tbody>{info.children && info.children.length > 0 ? info.children : noDataColumn()}</tbody>
      </table>
    </div>
  );
};

export default ColumnTable;
