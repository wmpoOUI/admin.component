import React from 'react';
import { ReactSortable } from 'react-sortablejs';
import cx from 'classnames';
import styles from './SortableTable.module.scss';

const SortableTable = props => {
  const {
    id = 'sortable',
    className,
    data = [],
    setData = () => {},
    columns = [],
    placeholder = '내용이 없습니다.',
    onRemove,
  } = props;
  const columnDefs = columns.map(column => {
    return {
      className: null,
      data: null,
      render: data => {
        return data || 'null';
      },
      width: null,
      ...column,
    };
  });
  columnDefs.unshift({
    className: styles.handle,
    data: null,
    render: () => {
      return (
        <>
          <i className="fa fa-bars" />
          <span className="screen-out">move</span>
        </>
      );
    },
    width: 1,
  });

  return (
    <div className={cx(styles.wrapper, { [className]: className })}>
      <table>
        <colgroup>
          {columnDefs.map((c, j) => {
            const { data, width } = c;
            const key = data ? `${data}-${j}` : `col-${j}`;
            return <col key={key} width={width} />;
          })}
          {onRemove && <col width="1" />}
        </colgroup>
        {data.length > 0 ? (
          <ReactSortable
            handle={`.${styles.handle}`}
            ghostClass={styles.ghost}
            tag="tbody"
            list={data}
            setList={data => setData(data)}
            onEnd={e => {
              e.item.style.transform = null;
            }}
          >
            {data.map((row, i) => {
              const meta = { row: i, settings: props };
              const key = `${row.ID}-${i}` || `${id}-${i}`;
              return (
                <tr key={key}>
                  {columnDefs.map((c, j) => {
                    const { className, data, render } = c;
                    const key = data ? `${data}-${j}` : `col-${j}`;
                    return (
                      <td key={key} className={className}>
                        {render(row[data], row, { col: j, ...meta })}
                      </td>
                    );
                  })}
                  {onRemove && (
                    <td>
                      <button
                        type="button"
                        className={styles.remove}
                        onClick={() => onRemove(data, row, { col: columnDefs.length + 1, ...meta })}
                      >
                        <i className="fa fa-times" />
                        <span className="screen-out">삭제</span>
                      </button>
                    </td>
                  )}
                </tr>
              );
            })}
          </ReactSortable>
        ) : (
          <tbody>
            <tr>
              <td colSpan={columnDefs.length} className={styles.empty}>
                {placeholder}
              </td>
            </tr>
          </tbody>
        )}
      </table>
    </div>
  );
};

export default SortableTable;
