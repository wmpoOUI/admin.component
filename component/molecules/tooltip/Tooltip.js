import React, { useState } from 'react';
import cx from 'classnames';
import styles from './Tooltip.module.scss';

const Tooltip = ({ className, message, position, title, children, ...other }) => {
  const [show, setShow] = useState(false);

  const showTooltop = () => {
    setShow(true);
  };

  const hideTooltop = () => {
    setShow(false);
  };

  return (
    <span className={cx(styles.wrapper, { [styles.show]: show })}>
      <span
        className={cx(styles.text, { [className]: className })}
        onClick={showTooltop}
        onKeyPress={showTooltop}
        role="button"
        tabIndex="0"
        {...other}
      >
        {children}
      </span>
      <div className={cx(styles.tooltip, { [styles.show]: show })} data-position={position}>
        <div className={styles.arrow} />
        <div className={styles.head}>
          {title && <strong className={styles.title}>{title}</strong>}
          <button type="button" className={styles.close} onClick={hideTooltop}>
            <i className="fa fa-times" />
            <span className="screen-out">{`${title} `}툴팁 닫기</span>
          </button>
        </div>
        <div className={styles.body}>{message}</div>
      </div>
    </span>
  );
};

export default Tooltip;
