import React from 'react';
import cx from 'classnames';
import styles from './SelectScrollList.module.scss';

const SelectScrollList = ({ className, height, list, onChange }) => {
  return (
    <ul className={cx(`box-comm ${styles.box}`, { [className]: className })} style={{ height }}>
      {list.map((node, i) => {
        const { label, value, checked } = node;

        return (
          <li key={value || i} className="inp-group">
            <input type="text" value={label} readOnly />
            <button
              type="button"
              className={checked ? `btn-slate` : `btn-default`}
              onClick={e => onChange(e, i, checked)}
            >
              선택
            </button>
          </li>
        );
      })}
    </ul>
  );
};

export default SelectScrollList;
