import React, { useEffect } from 'react';
import { Prompt as Confirm } from 'react-router-dom';

const Prompt = ({
  when = false,
  message = '페이지 이동 시 변경 사항이 저장되지 않습니다. 페이지를 이동하시겠습니까?',
}) => {
  useEffect(() => {
    const handleAction = e => {
      e.preventDefault();
      e.returnValue = message;
    };

    if (when) {
      window.addEventListener('beforeunload', handleAction);
      return () => window.removeEventListener('beforeunload', handleAction);
    }

    window.removeEventListener('beforeunload', handleAction);
  }, [when, message]);

  return <Confirm when={when} message={message} />;
};

export default Prompt;
