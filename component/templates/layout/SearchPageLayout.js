import React from 'react';
import DefaultLayout from './DefaultLayout';
import styles from './SearchPageLayout.module.scss';

const SearchPageLayout = ({ path, children }) => {
  const createElement = key => {
    if (Array.isArray(children)) return children.map(node => node.key === key && node);
    if (children.key === key) return children;
    return null;
  };

  const searchForm = createElement('searchForm');
  const child = createElement('child');
  const modal = createElement('modal');

  return (
    <>
      <DefaultLayout path={path}>
        <div className={styles.row}>
          {searchForm}
          <article id="mArticle" className={styles.article}>
            {child}
          </article>
        </div>
      </DefaultLayout>
      {modal}
    </>
  );
};

export default SearchPageLayout;
