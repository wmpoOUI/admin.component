import React from 'react';
import DefaultLayout from './DefaultLayout';
import styles from './SearchPageLayout.module.scss';

/**
 * ListPageLayout
 * @description 목록 레이아웃
 * @param {string} title
 * @param {string} subtitle
 * @param {array} path
 */
const ListPageLayout = ({ title, subtitle, path, children }) => {
  const createElement = key => {
    if (Array.isArray(children)) return children.map(node => node.key === key && node);
    if (children.key === key) return children;
    return null;
  };

  const searchForm = createElement('searchForm');
  const tool = createElement('tool');
  const table = createElement('table');
  const pagination = createElement('pagination');
  const foot = createElement('foot');
  const modal = createElement('modal');

  return (
    <>
      <DefaultLayout path={path}>
        <div className={styles.row}>
          {searchForm}
          <article id="mArticle" className={styles.article}>
            <div className="panel-comm">
              <div className="head-panel">
                <h2 className="tit-panel">
                  <i className="fa fa-search" /> {title} {subtitle && <small className="txt-muted">{subtitle}</small>}
                </h2>
                {tool}
              </div>
              <div className="body-panel">
                {table}
                {pagination}
              </div>
              {foot}
            </div>
          </article>
        </div>
      </DefaultLayout>
      {modal}
    </>
  );
};

export default ListPageLayout;
